package casino;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GameEngineTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PersonRepository personRepository;

    private Person person;

    @Before
    public void initPerson() {
        person = new Person();
        person.setFirstName("Lol");
        person.setLastName("Kekovich");
        person.setEmail("lol@kek.ru");
        person = personRepository.save(person);
    }

    @After
    public void clearDb() {
        personRepository.deleteAll();
    }

    @Test
    public void shouldBeAbleToBuyChips() throws Exception {
        String content = "{\"userId\":\"" + person.getId() + "\", \"chips\":\"100\"}";

        mockMvc.perform(post("/casino/buy")
                .header("Content-Type", "application/json")
                .content(content))
                .andExpect(status().isOk());

        Person newPers = personRepository.findById(person.getId()).get();
        Assert.assertEquals(100, newPers.getChips());
    }

    @Test
    public void shouldNotBeAbleToBuyNegativeAmountOfChips() throws Exception {
        String content = "{\"userId\":\"" + person.getId() + "\", \"chips\":\"-100\"}";

        mockMvc.perform(post("/casino/buy")
                .header("Content-Type", "application/json")
                .content(content))
                .andExpect(status().is4xxClientError());
    }
}
