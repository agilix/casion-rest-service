package casino;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GamerComponentTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PersonRepository personRepository;

    public Person createTestPerson() {
        return createTestPerson("Lol", "Kekovich", "lol@kek.ru");
    }

    public Person createTestPerson(String firstName, String lastName, String email) {
        Person person = new Person();
        person.setFirstName(firstName);
        person.setLastName(lastName);
        person.setEmail(email);
        personRepository.save(person);
        return person;
    }

    @Test
    public void shouldCreateEntity() throws Exception {
        String content = "{\"firstName\": \"Frodo\", \"lastName\":\"Baggins\"}";

        mockMvc.perform(
                post("/user")
                        .header("Content-Type", "application/json")
                        .content(content)
        )
                .andExpect(status().isCreated());
        assertEquals("Frodo", personRepository.findByLastName("Baggins").get(0).getFirstName());
    }

    @Test
    public void shouldBlockUser() throws Exception {
        createTestPerson("Frodo", "Baggins", "Test");

        Person person = personRepository.findByLastName("Baggins").get(0);

        mockMvc.perform(patch("/user/" + person.getId()).param("blocked", "true"))
                .andExpect(status().isOk());

        Person personBlocked = personRepository.findByLastName("Baggins").get(0);
        assertTrue(personBlocked.isBlocked());
    }

    @Test
    public void shouldReturnError() throws Exception {
        createTestPerson("Frodo", "Baggins", "Test");

        Person person = personRepository.findByLastName("Baggins").get(0);

        mockMvc.perform(patch("/user/" + person.getId()).param("blocked", "dsag"))
                .andExpect(status().isBadRequest());

        Person personBlocked = personRepository.findByLastName("Baggins").get(0);
        assertFalse(personBlocked.isBlocked());
    }

    @Test
    public void shouldNotFound() throws Exception {
        createTestPerson("Frodo", "Baggins", "Test");

        mockMvc.perform(patch("/user/643").param("blocked", "true"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldNotDoAnythingWithAlreadyBlockedUser() throws Exception {
        createTestPerson("Frodo", "Baggins", "Test");

        Person person = personRepository.findByLastName("Baggins").get(0);

        mockMvc.perform(patch("/user/" + person.getId()).param("blocked", "true"))
                .andExpect(status().isOk());

        Person personBlocked = personRepository.findByLastName("Baggins").get(0);
        assertTrue(personBlocked.isBlocked());

        String result = mockMvc.perform(patch("/user/" + person.getId()).param("blocked", "true"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertTrue(result.contains("is already blocked"));

    }

    @Test
    public void shouldSearchUserByEmail() throws Exception {
        Person person = createTestPerson();

        mockMvc.perform(
                get("/user/search")
                        .param("email", person.getEmail())
                        .header("Content-Type", "application/json")
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].firstName").value(person.getFirstName()))
                .andExpect(jsonPath("$[0].lastName").value(person.getLastName()))
                .andExpect(jsonPath("$[0].email").value(person.getEmail()));
    }

    @Test
    public void shouldDeleteUser() throws Exception {
        Person person = createTestPerson("Frodo", "Baggins", "Test");

        mockMvc.perform(delete("/user/" + person.getId()))
                .andExpect(status().isOk());

        Optional<Person> personDeleted = personRepository.findById(person.getId());
        assertTrue(personDeleted.isPresent() && personDeleted.get().isDeleted());

    }

    @Test
    public void shouldNotDeleteNonExisting() throws Exception {
        mockMvc.perform(delete("/user/643"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldGetUserEntityById() throws Exception {
        Person person = createTestPerson();
        person.setFirstName("John");
        person.setLastName("Bloch");
        person.setEmail("test@gmail.com");
        personRepository.save(person);

        mockMvc.perform(
                get("/find-user/" + person.getId())
        ).andExpect(
                status().isOk());

        assertEquals("John", personRepository.findById(person.getId()).get().getFirstName());

        personRepository.delete(person);
    }

    @Test
    public void shouldLogoutUser() throws Exception {
        Person person = createTestPerson();
        person.setFirstName("John");
        person.setLastName("Bloch");
        person.setEmail("test@gmail.com");
        person.setInGame(true);
        person = personRepository.save(person);

        mockMvc.perform(
                put("/user-logout/" + person.getId())
        ).andExpect(
                status().isOk());

        assertEquals(false, personRepository.findById(person.getId()).get().isInGame());

        personRepository.delete(person);
    }


    @Test
    public void shouldNotDeleteAlreadyDeleted() throws Exception {
        Person person = createTestPerson("Frodo", "Baggins", "Test");

        mockMvc.perform(delete("/user/" + person.getId()))
                .andExpect(status().isOk());

        mockMvc.perform(delete("/user/" + person.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldNotFindAlreadyDeleted() throws Exception {
        Person person = createTestPerson("Frodo", "Baggins", "Delete");

        mockMvc.perform(delete("/user/" + person.getId()))
                .andExpect(status().isOk());

        mockMvc.perform(
                get("/user/search")
                        .param("email", person.getEmail())
                        .header("Content-Type", "application/json"))
                .andExpect(status().isNotFound());
    }

}
