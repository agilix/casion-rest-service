package casino;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PersonControllerTest {

    private PersonController personController;

    private PersonRepository personRepositoryMock;

    @Before
    public void setUp() throws Exception {
        personRepositoryMock = mock(PersonRepository.class);
        personController = new PersonController(personRepositoryMock);
    }

    @Test
    public void shouldReturnAllPersons() {
        when(personRepositoryMock.findAll()).thenReturn(Arrays.asList(new Person(), new Person()));

        List<Person> list = personController.getAllPersons();

        assertEquals(2, list.size());
    }

    @Test
    public void shouldJoinToGame(){
        Long userId = 1L;
        when(personRepositoryMock.findById(eq(userId))).thenReturn(Optional.of(new Person()));

        personController.joinGame(userId);
        Person person = personController.findUser(userId).getBody();

        assertTrue(person.isInGame());
    }
}