package casino;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface PersonRepository extends PagingAndSortingRepository<Person, Long> {

    List<Person> findByLastName(String name);

    @Query(value = "select p from Person p where " +
            "(:firstName is null or p.firstName = :firstName) and " +
            "(:lastName is null or p.lastName = :lastName) and " +
            "(:email is null or p.email = :email) and (p.isDeleted = false)")
    List<Person> search(@Param("firstName") String firstName,@Param("lastName") String lastName,@Param("email") String email);
}