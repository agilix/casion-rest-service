package casino;

import javax.validation.constraints.Positive;

public class BuyChipsRequestDto {

    private long userId;
    @Positive
    private long chips;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getChips() {
        return chips;
    }

    public void setChips(long chips) {
        this.chips = chips;
    }
}
