package casino;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class CasinoController {

    @Autowired
    private PersonRepository repository;

    @PostMapping("/casino/buy")
    public ResponseEntity buyChips(@Valid @RequestBody BuyChipsRequestDto requestDto) {
        Person person = repository.findById(requestDto.getUserId()).get();
        person.setChips(person.getChips() + requestDto.getChips());
        repository.save(person);
        return ResponseEntity.ok().build();
    }
}
