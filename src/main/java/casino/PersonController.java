package casino;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    public PersonController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @GetMapping("/users")
    public List<Person> getAllPersons() {
        List <Person> persons = new ArrayList<>();
        personRepository.findAll().forEach(persons::add);
        return persons;
    }

    @PostMapping("/user")
    public ResponseEntity<Object> createUser(@RequestBody Person person) {
        Person savedPerson = personRepository.save(person);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedPerson.getId()).toUri();

        return ResponseEntity.created(location).build();

    }

    @PatchMapping("/user/{id}")
    public ResponseEntity<Object> blockUser(@PathVariable("id") long id, @RequestParam("blocked") Boolean blocked) {
        if (!Boolean.TRUE.equals(blocked)) {
            return ResponseEntity.badRequest().build();
        }
        Optional<Person> optionalPerson = personRepository.findById(id);
        if (!optionalPerson.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Person person = optionalPerson.get();
        if (!person.isBlocked()) {
            person.setBlocked(true);
            personRepository.save(person);
            return ResponseEntity.ok().body("User " + id + " blocked");
        } else {
            return ResponseEntity.ok().body("User " + id + " is already blocked");
        }
    }

    @DeleteMapping("/user/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable("id") long id) {
        Optional<Person> optionalPerson = personRepository.findById(id);
        if (!optionalPerson.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Person person = optionalPerson.get();
        if (!person.isDeleted()) {
            person.setDeleted(true);
            personRepository.save(person);
            return ResponseEntity.ok().body("User " + id + " deleted");
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/user/search")
    public ResponseEntity<Object> searchUser(@RequestParam(required = false) String firstName,
                                   @RequestParam(required = false) String lastName,
                                   @RequestParam(required = false) String email){
        List<Person> list = personRepository.search(firstName, lastName, email);
        if (list.isEmpty()) return ResponseEntity.notFound().build();
        else return ResponseEntity.ok().body(list);
    }

    @GetMapping("/find-user/{id}")
    public ResponseEntity<Person> findUser(@PathVariable("id") Long id) {
        Optional<Person> person = personRepository.findById(id);

        return person.isPresent()? ResponseEntity.ok(person.get()) : ResponseEntity.notFound().build();

    }

    @GetMapping("/joinGame/{id}")
    public ResponseEntity<Person> joinGame(Long id) {
        Optional<Person> personOptional = personRepository.findById(id);
        if (!personOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Person person = personOptional.get();
        person.setInGame(true);
        personRepository.save(person);
        return ResponseEntity.ok(person);
    }

    @PutMapping("/user-logout/{id}")
    public ResponseEntity<Boolean> logout(@PathVariable("id") long id) {
        Optional<Person> person = personRepository.findById(id);
        if (person.isPresent()) {
            person.get().setInGame(false);
            personRepository.save(person.get());
            return ResponseEntity.ok(true);
        }
        return ResponseEntity.badRequest().build();
    }
}
